package com.budeal.lucasapp;
import java.util.HashMap;

import android.util.Log;


/**
 * Budeal est une Startup qui développe une application mobile gratuite
 * permettant aux particuliers d'acheter et de vendre des produits 
 * d'occasion rapidement, simplement et en toute confiance. 
 *
 * http://budeal.com/app
 */


public class Developer
{
    protected boolean lookingForInternship = true; // Change accordingly
 
    public Developer(HashMap<String, Integer> skills)
    {
        // Presentations
        Log.d(Budeal.TAG, "Nous sommes une équipe de 3 personnes : " +
                          "Stephane dirige la société et s'assure d'avoir les moyens de la faire grandir, " +
                          "Damien  s'occupe du design et moi même, Jérôme " + 
                          "je développe les applications mobiles et peaufine le backend (Parse.com)");
              
        Log.i(Budeal.TAGLUCAS, "Salut les gars," +
        		"je développe des applications mobiles (surtout) depuis plus d'un an"+
        		"mais je connais aussi pas mal d'autres langages, et me définit plutôt comme un développeur d'applications !" +
        		"Je cherche aujourd'hui une alternance pour deux ans avec deux tiers de mon temps en entreprise");
        // Check skills
        if (canReadThisCode() && lookingForInternship) {
            if (skills.get("androidLevel") > 7) { // Level is from 0 to 10 (10 is the best)
                // Say welcome
                Log.d(Budeal.TAG, "Nous recherchons quelqu'un qui a de bonnes connaissances en développement Android " +
                                  "afin de continuer à faire évoluer la version Android de Budeal.");
                Log.i(Budeal.TAGLUCAS, "Comme je l'ai dit ça fait maintenant depuis avril 2014 que je développe sur mobile" +
                		"avec un an de développement sur Android en natif"+
                		"en avançant et développant des applications pour WePopp et Claria");
                if (skills.get("androidVersion") >= 5) {
                    Log.d(Budeal.TAG, "C'est un plus si tu connais Android Lolipop !");
                    Log.i(Budeal.TAGLUCAS, "Je connais Lollipop, " +
                    		"Mais étant donné la rétrocompatibilité c'est surtout important de savoir développer pour "
                    		+ "les version antérieures" + " pour toucher un max de gens !");
                }
            }
        }
    }
    
	protected boolean canReadThisCode() 
    {
        return true;
    }
    
	public String getQualities(){
		return "Créativité, autonomie, humour, rigueur, perfectionniste";
	}

	public String getSkills() {
		return "Android, Java, SQL, C#, Python, Gradle, méthodes agiles, bonne capacité de rédaction...";
	}
}
