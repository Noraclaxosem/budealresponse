package com.budeal.lucasapp;

import java.util.HashMap;

import com.example.gittestingandroid.R;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends ActionBarActivity {
	private static final boolean DEBUG = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if(DEBUG)
			Log.d(Budeal.TAG, "onResume");
		HashMap<String, Integer> skillsLevel = new HashMap<String,Integer>();
		skillsLevel.put("androidLevel", 8);
		skillsLevel.put("androidVersion",5);
		Developer budealFuturInternship = new Developer(skillsLevel);
		Log.i(Budeal.TAGLUCAS + " qualities",budealFuturInternship.getQualities());
		Log.i(Budeal.TAGLUCAS + " skills",budealFuturInternship.getSkills());
	}
}
